<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Helper;

//
use ReflectionClass;

use function strtoupper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait AsiConfigHelperTrait {
	
	/**
	 * @param    string         $name
	 * @param    null|object    $class
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getClassConstant(string $name, ?object $class = NULL) : mixed {
		return ( new ReflectionClass($class ?? $this) )->getConstant(strtoupper($name)) ?? FALSE;
	}
}
