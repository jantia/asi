<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Helper;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Standard\Asi\AsiPipelineHelperInterface;
use Tiat\Collection\Pipeline\Pipeline;
use Tiat\Standard\Pipeline\PipelineInterface;

use function is_string;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait AsiPipelineHelper {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_PIPELINE_INTERFACE = Pipeline::class;
	
	/**
	 * @var PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	private PipelineInterface $_pipelineInterface;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_pipelineInterfaceDefault = self::DEFAULT_PIPELINE_INTERFACE;
	
	/**
	 * @param    callable|object    $process
	 * @param    array              $options
	 * @param    string             $method
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function usePipeline(callable|object $process, array $options = [], string $method = 'run') : bool {
		// Get the pipeline
		if(( $pipe = $this->setPipeline(NULL, ...$options)->getPipeline() ) !== NULL):
			// Register the process to the pipeline
			$p = $pipe->registerPipe($process, $method, ...$options)->run();
			
			//
			if($p instanceof PipelineInterface):
				return $p->hasRun();
			endif;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return null|PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipeline() : ?PipelineInterface {
		return $this->_pipelineInterface ?? NULL;
	}
	
	/**
	 * @param    null|string|PipelineInterface    $pipeline
	 * @param    mixed                            ...$args
	 *
	 * @return AsiPipelineHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipeline(PipelineInterface|string|null $pipeline = NULL, ...$args) : AsiPipelineHelperInterface {
		//
		$interface = $this->getPipelineDefault($pipeline, ...$args);
		
		//
		if($interface instanceof PipelineInterface):
			$this->_pipelineInterface = $interface;
		else:
			$msg = sprintf("Pipeline must be instance of %s.", PipelineInterface::class);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Get pipeline interface
	 *
	 * @param    null|string|PipelineInterface    $pipeline
	 * @param    mixed                            ...$args
	 *
	 * @return null|PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipelineDefault(PipelineInterface|string|null $pipeline = NULL, ...$args) : ?PipelineInterface {
		//
		if($pipeline === NULL):
			$default   = $this->getPipelineInterfaceDefault();
			$interface = new $default([], ...$args);
		elseif(is_string($pipeline)):
			$interface = new $pipeline([], ...$args);
		else:
			$interface = $pipeline;
		endif;
		
		//
		return $interface;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipelineInterfaceDefault() : string {
		return $this->_pipelineInterfaceDefault;
	}
	
	/**
	 * @param    string    $interface
	 *
	 * @return AsiPipelineHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipelineInterfaceDefault(string $interface) : AsiPipelineHelperInterface {
		//
		$this->_pipelineInterfaceDefault = $interface;
		
		//
		return $this;
	}
	
	/**
	 * @return AsiPipelineHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetPipeline() : AsiPipelineHelperInterface {
		//
		unset($this->_pipelineInterface);
		
		//
		return $this;
	}
}
