<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Helper;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Register\AsiRegisterHelper;
use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Standard\Asi\AsiProcessInterface;
use Jantia\Standard\Platform\PlatformEnv;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Pipeline\PipelineInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Stdlib\Loader\ClassLoaderTrait;
use Tiat\Stdlib\Register\RegisterContainerInterface;
use WeakMap;

use function define;
use function defined;
use function get_debug_type;
use function is_callable;
use function is_object;
use function key;
use function sprintf;

/**
 * This class is a helper for ASI model process. Class will read RegisterContainerInterface and parse it and run the process.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait AsiProcessHelperTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait {
		ClassLoaderTrait::run as classLoaderRun;
	}
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiHelperInterfaceTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiPipelineHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiRegisterHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiConfigHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string TRACE_ID_PREFIX = 'HTTP_X_JANTIA_';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array PROCESS_ORDER = [AsiRegisterParams::PROCESS->value, AsiRegisterParams::NEXT_LAYER->value];
	
	/**
	 * All ASI interface's & layer's must be an instance of this
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_ASI_INTERFACE = AsiProcessInterface::class;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private readonly string $_configFile;
	
	/**
	 * @var WeakMap
	 * @since   3.0.0 First time introduced.
	 */
	private WeakMap $_pipeResult;
	
	/**
	 * Main function to run the process.
	 *
	 * @param    RegisterInterface|array    $register
	 * @param                               ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function processRegister(RegisterInterface|array $register, ...$args) : void {
		// Try to find register containers and parse them in given order.
		if($register instanceof RegisterInterface):
			if(! empty($containers = $register->getRegisterContainers())):
				// There is always only one container in register
				$key   = key($containers);
				$value = $containers[$key];
				
				// Phase #1: parse all layer(s) at first
				if(! empty($rc = $this->_getRegisterSection($value, AsiRegisterParams::NEXT_LAYER->value))):
					$this->_parseLayer($rc);
				endif;
				
				// Phase #2: parse interface main process
				if(! empty($rc = $this->_getRegisterSection($value, AsiRegisterParams::PROCESS->value)) &&
				   ! empty($callables = $this->_parseProcess($rc))):
					$this->_parseCallables([$key => $callables], $value);
				endif;
				
				// Phase #3: next interface
				if(! empty($iface = $this->_getRegisterSection($value, AsiRegisterParams::NEXT_INTERFACE->value))):
					$this->_parseInterface($iface);
				endif;
			endif;
		else:
			foreach($register as $r):
				$this->processRegister($r);
			endforeach;
		endif;
	}
	
	/**
	 * @param    RegisterContainerInterface    $register
	 * @param    string                        $section
	 *
	 * @return null|mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getRegisterSection(RegisterContainerInterface $register, string $section) : ?RegisterContainerInterface {
		if(( $r = $register->searchValue($section) ) !== NULL):
			return $r;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    RegisterContainerInterface    $register
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _parseLayer(RegisterContainerInterface $register) : void {
		if(! empty($result = $register->getParams())):
			foreach($result as $key => $value):
				// Get the process section
				if(( $reg = $this->_getRegisterSection($value, AsiRegisterParams::PROCESS->value) ) !== NULL):
					// Define callables
					$proc = $this->_parseProcess($reg);
					$this->_parseCallables([$key => $proc], $value);
				endif;
			endforeach;
		endif;
	}
	
	/**
	 * Create callable from process section.
	 *
	 * @param    RegisterContainerInterface    $register
	 *
	 * @return null|callable
	 * @since   3.0.0 First time introduced.
	 */
	protected function _parseProcess(RegisterContainerInterface $register) : ?array {
		return [$register];
	}
	
	/**
	 * @param    array                         $callables
	 * @param    RegisterContainerInterface    $register
	 * @param    bool                          $bundle
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _parseCallables(array $callables, RegisterContainerInterface $register, bool $bundle = TRUE) : void {
		if(! empty($callables)):
			// Move callable(s) to pipeline
			if(( $pipeline = $this->_getRegisterSection($register, AsiRegisterParams::PIPELINE->value) ) !== NULL):
				foreach($callables as $key => $callable):
					$this->_parsePipeline($pipeline, [$key => $callable], $bundle);
				endforeach;
			else:
				throw new InvalidArgumentException("Callable implementation is missing because pipelines are used as default.");
			endif;
		endif;
	}
	
	/**
	 * @param    RegisterContainerInterface    $register
	 * @param    array                         $callables
	 * @param    bool                          $bundle
	 *
	 * @return null|WeakMap
	 * @since   3.0.0 First time introduced.
	 */
	protected function _parsePipeline(RegisterContainerInterface $register, array $callables, bool $bundle = TRUE) : ?WeakMap {
		// Reset the pipeline
		$pipeline = NULL;
		
		// Get pipeline settings if exists
		if(( $register->getParam(AsiRegisterParams::PIPELINE_USE->value) ) === TRUE):
			// Define the pipeline settings
			$pipeBefore = $register->getParam(AsiRegisterParams::PIPELINE_BEFORE->value);
			$pipeAfter  = $register->getParam(AsiRegisterParams::PIPELINE_AFTER->value);
			$pipeMethod = $register->getParam(AsiRegisterParams::PIPELINE_METHOD->value);
			
			//
			foreach($callables as $name => $array):
				//
				if(! empty($array)):
					foreach($array as $params):
						// Get (new) pipeline
						if($pipeline === NULL):
							$p        = ['autoinit' => TRUE];
							$pipeline = $this->getPipelineDefault($this->getPipelineInterfaceDefault(), ...$p);
						endif;
						
						// Declare new instance from callables & run the callable
						if(is_callable($callable = $this->declareCallable($params))):
							// Register callable in pipeline
							$pipeline->registerPipe($callable, $pipeMethod ?? NULL);
							
							// Register callable in pipeline if there is a before callable
							if(! empty($pipeBefore) && is_callable($pipeBefore)):
								$this->_registerPipelineCallable($pipeline, $callable, $pipeBefore, 'before');
							endif;
							
							// Register callable in pipeline if there is an after callable
							if(! empty($pipeAfter) && is_callable($pipeAfter)):
								$this->_registerPipelineCallable($pipeline, $callable, $pipeAfter, 'after');
							endif;
							
							// Save each pipe
							if(is_object($callable)):
								$this->_setPipelineResult((object)$callable, $name);
							else:
								throw new InvalidArgumentException(sprintf("%s is not callable object.",
								                                           get_debug_type($callable)));
							endif;
							
							// Run all one-by-one and get the result
							if($bundle === FALSE):
								$pipeline->run();
								$pipeline->unregisterPipe($callable);
							endif;
						else:
							throw new InvalidArgumentException(sprintf("%s is not callable.",
							                                           get_debug_type($callable)));
						endif;
					endforeach;
				endif;
			endforeach;
			
			// Run all as bundle
			if($pipeline !== NULL && $bundle === TRUE):
				$pipeline->run();
			endif;
			
			//
			return $this->_getPipelineResult();
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    RegisterContainerInterface    $register
	 *
	 * @return null|callable
	 * @since   3.0.0 First time introduced.
	 */
	public function declareCallable(RegisterContainerInterface $register) : ?callable {
		// Get handler, contract, args, settings from register.
		$handler  = $register->getParam(AsiRegisterParams::HANDLER->value);
		$contract = $register->getParam(AsiRegisterParams::CONTRACT->value);
		$args     = $register->getParam(AsiRegisterParams::PARAMS->value);
		$settings = $register->getParam(AsiRegisterParams::SETTINGS->value);
		
		// Add callable values inside the array which will be declared & called later.
		// Callable have to be an instance of AsiProcessInterface
		if($handler !== NULL && ( $callable = new $handler(...$args) ) instanceof AsiProcessInterface &&
		   is_callable($callable)):
			//
			if($contract !== NULL && ! $callable instanceof $contract):
				$msg = sprintf("Handler %s is not an instance of %s.", $handler, $contract);
				throw new InvalidArgumentException($msg);
			endif;
			
			// Parse settings if exists
			if(! empty($settings) && $settings instanceof ParametersPluginInterface):
				$this->setSettings($callable, $settings->getParams());
			endif;
			
			// Return single callable in array
			return $callable;
		else:
			throw new RuntimeException("Process has been defined but callable can't be created.");
		endif;
	}
	
	/**
	 * @param    callable|object    $pipeline
	 * @param    callable           $pipe
	 * @param    callable           $value
	 * @param    string             $type
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _registerPipelineCallable(callable|object $pipeline, callable $pipe, callable $value, string $type) : void {
		if($pipeline instanceof PipelineInterface):
			match ( $type ) {
				'after' => $pipeline->after($pipe, $value),
				'before' => $pipeline->before($pipe, $value)
			};
		else:
			throw new InvalidArgumentException("Pipeline must be an instance of PipelineInterface.");
		endif;
	}
	
	/**
	 * @param    object    $pipe
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setPipelineResult(object $pipe, string $name) : void {
		if(empty($this->_pipeResult)):
			$this->_pipeResult = new WeakMap();
		endif;
		
		//
		$this->_pipeResult->offsetSet($pipe, $name);
	}
	
	/**
	 * @return null|WeakMap
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getPipelineResult() : ?WeakMap {
		return $this->_pipeResult ?? NULL;
	}
	
	/**
	 * @param    RegisterContainerInterface    $register
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _parseInterface(RegisterContainerInterface $register) : void {
		// Register includes Register name which must be opened for next interface.
		if(! empty($params = $register->getParams())):
			//
			foreach($params as $key => $name):
				// Get named register
				if(( $class = $this->getNamedRegister($name) ) !== NULL):
					$result[$key] = $class;
					$class        = NULL;
				endif;
			endforeach;
			
			// Parse interface
			if(! empty($result)):
				$this->processRegister($result);
			endif;
		endif;
	}
	
	/**
	 * Set the TRACE_ID in the environment.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setPlatform() : void {
		//
		$name = self::TRACE_ID_PREFIX . PlatformEnv::TRACE_ID->value;
		
		//
		if(! empty($id = $_SERVER[$name] ?? NULL) && ! defined(PlatformEnv::TRACE_ID->value)):
			define(PlatformEnv::TRACE_ID->value, $id);
		endif;
		
		// Throw error if trace id is still missing.
		if(! defined(PlatformEnv::TRACE_ID->value)):
			$msg = sprintf("Missing %s trace id. Please define HTTP request value.", $name);
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConfigFile() : string {
		return $this->_configFile;
	}
	
	/**
	 * @param    string    $filename
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigFile(string $filename) : void {
		$this->_configFile = $filename;
	}
}
