<?php

/**
 * /**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Helper;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Standard\Asi\AsiHelperInterface;
use Psr\Log\LoggerInterface;
use Tiat\Collection\MessageBus\MessageBusInterface;

use function ctype_graph;
use function sprintf;

/**
 * Include this with all ASI Interfaces
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait AsiHelperInterfaceTrait {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_defaultInterfaceHandler;
	
	/**
	 * @var LoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	private LoggerInterface $_logInterface;
	
	/**
	 * @var AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	private AsiHelperInterface $_interfaceHandler;
	
	/**
	 * @var MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	private MessageBusInterface $_messageBusInterface;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameApplication;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_monitoringStatus = FALSE;
	
	/**
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : void {
		//
		if(! empty($name) && $this->checkName($name) === TRUE):
			$this->_nameApplication = $name;
		else:
			$msg = sprintf("Given string (%s') does not consist of all (visibly) printable characters", $name);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkName(string $name) : bool {
		return ctype_graph($name);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_nameApplication ?? NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkLog() : bool {
		//
		if(! empty($this->_logInterface)):
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * Get PSR Log (interface) from the Layer
	 *
	 * @return null|LoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getLog() : ?LoggerInterface {
		return $this->_logInterface ?? NULL;
	}
	
	/**
	 * Set PSR Log (interface) for the Layer
	 *
	 * @param    LoggerInterface    $logger
	 *
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setLog(LoggerInterface $logger) : AsiHelperInterface {
		//
		$this->setLogger($logger);
		
		//
		return $this;
	}
	
	/**
	 * @param    LoggerInterface    $logger
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setLogger(LoggerInterface $logger) : void {
		$this->_logInterface = $logger;
	}
	
	/**
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetLog() : AsiHelperInterface {
		//
		unset($this->_logInterface);
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkMessageBus() : bool {
		//
		if(! empty($this->_messageBusInterface)):
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return null|MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessageBus() : ?MessageBusInterface {
		return $this->_messageBusInterface ?? NULL;
	}
	
	/**
	 * @param    MessageBusInterface    $messageBus
	 *
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessageBus(MessageBusInterface $messageBus) : AsiHelperInterface {
		//
		$this->_messageBusInterface = $messageBus;
		
		//
		return $this;
	}
	
	/**
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessageBus() : AsiHelperInterface {
		//
		unset($this->_messageBusInterface);
		
		//
		return $this;
	}
}
