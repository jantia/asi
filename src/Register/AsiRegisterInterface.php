<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Register;

//
use Psr\Log\LoggerInterface;
use Tiat\Collection\MessageBus\MessageBusInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Register\RegisterContainerInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiRegisterInterface extends AsiRegisterHelperInterface, RegisterInterface, RegisterPluginInterface {
	
	/**
	 * Try to get system default LoggerInterface
	 *
	 * @return null|LoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultLogger() : LoggerInterface|null;
	
	/**
	 * Get default register with application system constant values
	 *
	 * @param    bool    $throwError
	 *
	 * @return null|(RegisterPluginInterface&ParametersPluginInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultRegister(bool $throwError = FALSE) : ( ParametersPluginInterface&RegisterPluginInterface )|null;
	
	/**
	 * @param    string                      $name
	 * @param    bool                        $throwError
	 * @param    RegisterInterface|string    $register
	 *
	 * @return null|(RegisterPluginInterface&ParametersPluginInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public function getNamedRegister(string $name, bool $throwError, RegisterInterface|string $register) : ( ParametersPluginInterface&RegisterPluginInterface )|null;
	
	/**
	 * Get the application register container.
	 *
	 * @return null|RegisterContainerInterface The application register container
	 * @since   3.0.0 First time introduced.
	 */
	public function getApplicationRegister() : ?RegisterContainerInterface;
	
	/**
	 * Get the name of the application
	 *
	 * @return string The name of the application
	 * @since   3.0.0 First time introduced.
	 */
	public function getApplicationName() : string;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setApplicationName() : void;
	
	/**
	 * Get the server URL.
	 *
	 * @return null|string The server URL or null if not available.
	 * @since   3.0.0 First time introduced.
	 */
	public function getServerUrl() : ?string;
	
	/**
	 * Get default Message Bus from general register.
	 *
	 * @return null|MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessageBusDefault() : ?MessageBusInterface;
}
