<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Register;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiRegisterHelperInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_REGISTER_VALUE = AsiRegisterParams::DEFAULT_REGISTER->value;
	
	/**
	 * Define the constant for server URL.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVER_URL_VALUE = 'SERVER_URL';
	
	/**
	 * Name of the constant where is the default server name pattern.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string URL_SECTIONS_NAME = 'URL_SECTIONS';
	
	/**
	 * Section value starts with this value.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string URL_SECTIONS_START = 'URL_SECTION_';
	
	/**
	 * Define the language section name.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string URL_SECTIONS_LANGUAGE = 'lang';
	
	/**
	 * Define the domain section pattern.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string URL_SECTIONS_PATTERN = 'URL_SECTIONS_PATTERN';
	
	/**
	 * Default server name.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string URL_SECTIONS_DEFAULT = "{server}.{domain}.{tld}";
}