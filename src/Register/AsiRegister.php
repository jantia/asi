<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Register;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Default register list for ASI Interface's & Layer's
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum AsiRegister: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_DEFAULT = 'REGISTER_DEFAULT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_INTERFACE_BOOTSTRAP = 'REGISTER_BOOTSTRAP';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_INTERFACE_BOOTSTRAP_OTHER = 'REGISTER_BOOTSTRAP_OTHER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_INTERFACE_TRANSFER = 'REGISTER_INTERFACE_TRANSFER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_INTERFACE_KERNEL = 'REGISTER_INTERFACE_KERNEL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_INTERFACE_OTHER = 'REGISTER_INTERFACE_OTHER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_INTERFACE_SOFTWARE = 'REGISTER_INTERFACE_SOFTWARE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_APPLICATION = 'REGISTER_LAYER_APPLICATION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_DATALINK = 'REGISTER_LAYER_DATALINK';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_NETWORK = 'REGISTER_LAYER_NETWORK';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_OTHER = 'REGISTER_LAYER_OTHER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_PHYSICAL = 'REGISTER_LAYER_PHYSICAL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_PRESENTATION = 'REGISTER_LAYER_PRESENTATION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER_LAYER_SESSION = 'REGISTER_LAYER_SESSION';
}
