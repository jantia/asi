<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Register;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Skeleton\Bootstrap\Config\DefaultConfigApplication;
use Jantia\Standard\Platform\PlatformEnv;
use Tiat\Standard\DataModel\HttpMethodCustom;
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\PhpType;
use Tiat\Standard\DataModel\TraitEnum;
use Tiat\Standard\Monitor\MonitorInterface;

use function sprintf;

/**
 * ASI register params.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum AsiRegisterParams: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ADAPTER = 'adapter';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case APPLICATION_NAME = PlatformEnv::ENV_APP_NAME->value;
	
	/**
	 * Callback for the
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CALLBACK = 'callback';
	
	/**
	 * Credentials file absolute path
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CREDENTIALS_FILE = 'credentials_file';
	
	/**
	 * Name of cert path constant in the application.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CRYPTO_CERT_PATH = 'crypto_cert_path';
	
	/**
	 * Custom verbs used.
	 *
	 * @see     HttpMethodCustom
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM_HTTP_VERBS = 'custom_http_verbs';
	
	/**
	 * Default register constant name
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case DEFAULT_REGISTER = 'default_register';
	
	/**
	 * Custom error handler for the application
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case ERROR_HANDLER = 'error_handler';
	
	/**
	 * Object which will handle the operation
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case HANDLER = 'handler';
	
	/**
	 * Next Asi Interface object to process
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case INTERFACE = 'interface';
	
	/**
	 * Set of interfaces that define the core services provided by the framework. This is used as a pair with self::HANDLER
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CONTRACT = 'contract_required';
	
	/**
	 * Used HTTP methods
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case HTTP_METHODS = 'http_methods';
	
	/**
	 * All Layers which interface should process
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LAYER = 'layer';
	
	/**
	 * Language value
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LANGUAGE = 'language';
	
	/**
	 * Define language settings for each module with module specified format.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LANGUAGE_SETTINGS = 'language_settings';
	
	/**
	 * Log handler object
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LOG_HANDLER = 'log_handler';
	
	/**
	 * Message bus interface
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case MESSAGE_BUS = 'message_bus';
	
	/**
	 * Monitor the given module with Tiat\Standard\Monitor\MonitorInterface
	 *
	 * @since   3.0.0 First time introduced.
	 * @see     MonitorInterface
	 */
	case MONITORING_STATUS = 'monitoring_status';
	
	/**
	 * Name of the
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case NAME = 'name';
	
	/**
	 * Next interface which are executed
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case NEXT_INTERFACE = 'next_interface';
	
	/**
	 * Next layer's to executed in current interface
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case NEXT_LAYER = 'next_layer';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case OPTIONS = 'options';
	
	/**
	 * Use this as $args if defined
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PARAMS = 'params';
	
	/**
	 * Pipeline settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PIPELINE = 'pipeline';
	
	/**
	 * Execute AFTER the main pipeline
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PIPELINE_AFTER = 'pipeline_after';
	
	/**
	 * Execute BEFORE the main pipeline
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PIPELINE_BEFORE = 'pipeline_before';
	
	/**
	 * What method is registered to use in pipeline
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PIPELINE_METHOD = 'pipeline_method';
	
	/**
	 * Use the pipeline (TRUE or FALSE)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PIPELINE_USE = 'pipeline_use';
	
	/**
	 * Process defined object. This is usually called at once, and it's the instance of Bootstrap which will call other interfaces internally.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PROCESS = 'process';
	
	/**
	 * Define the register instance
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case REGISTER = 'register';
	
	/**
	 * Application router model (MVC, MVVM...)
	 *
	 * @since   3.0.0 First time introduced.
	 * @see     RouterType
	 */
	case ROUTER_MODEL = 'router_model';
	
	/**
	 * Application router type
	 *
	 * @since   3.0.0 First time introduced.
	 * @see     RouterType
	 */
	case ROUTER_TYPE = 'router_type';
	
	/**
	 * Router plugin type (literal etc).
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case ROUTER_PLUGIN = 'router_plugin';
	
	/**
	 * Used for defined settings for AsiRegisterModules
	 *
	 * @since   3.0.0 First time introduced.
	 * @see     DefaultConfigApplication
	 */
	case SETTINGS = 'settings';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case VALUE = 'value';
	
	/**
	 * Used Validators
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case VALIDATORS = 'validators';
	
	/**
	 * Settings for the variables
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case VARIABLES = 'variables';
	
	/**
	 * Variables order. Example GET/POST/COOKIE/FILES/ENV/SERVER => 'gpcfes' or 'GPCFES'
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case VARIABLES_ORDER = 'variables_order';
	
	/**
	 * Return Backed enum value type in return for gettype() method
	 *
	 * @param    AsiRegisterParams    $value
	 *
	 * @return PhpType
	 * @since   3.0.0 First time introduced.
	 */
	public static function valueType(AsiRegisterParams $value) : PhpType {
		return match ( $value ) {
			self::CALLBACK => PhpType::CALLABLE,
			self::HANDLER, self::ERROR_HANDLER, self::OPTIONS, self::SETTINGS, self::VARIABLES => PhpType::ARRAY,
			self::ADAPTER, self::INTERFACE, self::LANGUAGE, self::LAYER, self::LOG_HANDLER, self::HTTP_METHODS, self::NEXT_INTERFACE, self::NEXT_LAYER, self::REGISTER => PhpType::STRING,
			self::CUSTOM_HTTP_VERBS, self::MONITORING_STATUS => PhpType::BOOL,
			self::VALUE => PhpType::MIXED,
			default => throw new InvalidArgumentException('Value must be one of backed enum values')
		};
	}
	
	/**
	 * @param    AsiRegisterParams    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public static function getDescription(AsiRegisterParams $value) : string {
		return match ( $value ) {
			self::CONTRACT => "Set of interfaces that define the core services provided by the framework.",
			self::CUSTOM_HTTP_VERBS => "Custom verbs used. * @see Tiat\Standard\DataModel\HttpMethodCustom",
			self::HTTP_METHODS => "Used HTTP methods",
			self::INTERFACE => "Next Asi Interface object to process",
			self::LAYER => "All Layers which interface should process",
			self::LOG_HANDLER => "Log handler object. Usually Psr\LoggerInterface",
			self::PROCESS => "Process the defined object. Use array[self::INTERFACE=>'', self::HANDLER=>''] format.",
			self::VARIABLES => "Settings for the variables",
			self::VARIABLES_ORDER => "Variables order. Example GET/POST/COOKIE/FILES/ENV/SERVER => 'gpcfes' or 'GPCFES'",
			default => throw new InvalidArgumentException(sprintf("There is no description for %s", $value->value))
		};
	}
}
