<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Register;

//
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Standard\Platform\PlatformEnv;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Tiat\Collection\MessageBus\MessageBusInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Register\RegisterContainer;
use Tiat\Stdlib\Register\RegisterContainerInterface;

use function constant;
use function define;
use function defined;
use function is_array;
use function is_string;
use function preg_replace;
use function str_starts_with;
use function strlen;
use function strtolower;
use function substr;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait AsiRegisterHelper {
	
	/**
	 * @return null|LoggerInterface
	 * @since  3.0.0 First time introduced.
	 * @since  3.0.1 Updated to use RegisterContainer
	 */
	public function getDefaultLogger() : LoggerInterface|null {
		// Get application name to get correct container
		if(( ( $register = $this->getDefaultRegister() ) !== NULL ) &&
		   ( $container = $register->getRegisterContainer(getenv(PlatformEnv::ENV_APP_NAME->value)) ) !== NULL):
			// Get log handler if exists
			return $container->getParam(AsiRegisterParams::LOG_HANDLER->value);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    bool    $throwError
	 *
	 * @return null|(RegisterPluginInterface&ParametersPluginInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultRegister(bool $throwError = FALSE) : ( ParametersPluginInterface&RegisterPluginInterface )|null {
		//
		if(defined(AsiRegisterParams::DEFAULT_REGISTER->value) &&
		   ! empty($name = constant(AsiRegisterParams::DEFAULT_REGISTER->value))):
			//
			return $this->getNamedRegister(self::DEFAULT_REGISTER_VALUE, throwError:$throwError);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string                      $name
	 * @param    bool                        $throwError
	 * @param    RegisterInterface|string    $register
	 *
	 * @return null|(RegisterPluginInterface&ParametersPluginInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public function getNamedRegister(string $name, bool $throwError = FALSE, RegisterInterface|string $register = self::DEFAULT_REGISTER_VALUE) : ( ParametersPluginInterface&RegisterPluginInterface )|null {
		//
		if(is_string($register)):
			if(defined($register) && ! empty($default = constant($register))):
				$class = new $default();
				if($register === $name):
					$name = $default;
				endif;
			else:
				$class = new $register();
			endif;
		else:
			$class = $register;
		endif;
		
		//
		if($class instanceof RegisterInterface):
			return $class::pullRegister(new $name());
		elseif($throwError === TRUE):
			throw new RuntimeException("Default register is not defined.");
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Return Register names from application register. This is used usually conjunction with getNamedRegister()
	 *
	 * @return null|RegisterContainerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getApplicationRegister() : ?RegisterContainerInterface {
		if(( ( $r = $this->getDefaultRegister() ) !== NULL ) &&
		   ( $regs = $r->getRegisterContainer($this->getApplicationName()) ) !== NULL &&
		   $regs instanceof RegisterContainer):
			//
			return $regs->getContainer(strtolower(AsiRegisterParams::REGISTER->value));
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getApplicationName() : string {
		return getenv(PlatformEnv::ENV_APP_NAME->value) ?? $_SERVER['HTTP_HOST'];
	}
	
	/**
	 * Set the application name.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setApplicationName() : void {
		// Get sections from application level or use the default one in this trait.
		$pattern = $this->getClassConstant(self::URL_SECTIONS_NAME);
		
		// Set the pattern as constant.
		if(! defined(self::URL_SECTIONS_PATTERN)):
			define(self::URL_SECTIONS_PATTERN, $pattern);
		endif;
		
		// Set the sections as constants.
		if(! empty($c = ( new ReflectionClass($this) )->getConstants())):
			foreach($c as $key => $val):
				if(str_starts_with($key, self::URL_SECTIONS_START)):
					$sections[strtolower(substr($key, strlen(self::URL_SECTIONS_START)))] = $val;
				endif;
			endforeach;
			
			//
			if(! empty($sections)):
				$this->_constructServerUrl(strtolower($pattern), $sections);
			endif;
		endif;
	}
	
	/**
	 * Construct the server URL based on the given pattern and sections.
	 *
	 * @param    string    $pattern
	 * @param    array     $sections
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _constructServerUrl(string $pattern, array $sections) : void {
		// Replace placeholders in the pattern with their respective values.
		foreach($sections as $key => $val):
			$pattern = str_replace('{' . $key . '}', $val, $pattern);
		endforeach;
		
		// Remove non-replaced placeholders in the pattern (e.g., {dc}, {domain}, {tld})
		if(is_array($final = preg_replace('/\{[^}]+\}/', '', $pattern))):
			$pattern = implode('.', $final);
		else:
			$pattern = (string)$final;
		endif;
		
		// Remove consecutive dots (.) in the pattern
		if(is_array($dots = preg_replace('/\.{2,}/', '.', $pattern))):
			$pattern = implode('', $dots);
		else:
			$pattern = (string)$dots;
		endif;
		
		// Remove leading and trailing dots
		$pattern = trim($pattern, '.');
		
		// Define the constant for server URL.
		if(! defined(self::SERVER_URL_VALUE)):
			define(self::SERVER_URL_VALUE, $pattern);
		endif;
	}
	
	/**
	 * Return server URL from the constant if defined, otherwise return null.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getServerUrl() : ?string {
		if(defined(self::SERVER_URL_VALUE)):
			return constant(self::SERVER_URL_VALUE);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return null|MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessageBusDefault() : ?MessageBusInterface {
		//
		if(( $r = $this->getDefaultRegister() ) !== NULL):
			//
			$container = $r->getRegisterContainer($this->getApplicationName());
			if($container instanceof RegisterContainerInterface):
				//
				$bus = $container->getParam(AsiRegisterParams::MESSAGE_BUS->value);
				
				if(is_string($bus)):
					$bus = new $bus();
				endif;
				
				//
				if($bus instanceof MessageBusInterface):
					return $bus;
				endif;
			endif;
		endif;
		
		//
		return NULL;
	}
}
