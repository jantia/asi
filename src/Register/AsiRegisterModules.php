<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Register;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Register Tiat\Framework module names for ASI model. Use module name with [] as $args for each module.
 * Example ROUTER => [AsiRegisterParams::NAME->value => 'your_setting_for_router_module']
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum AsiRegisterModules: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case RESPONSE = 'RESPONSE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REQUEST = 'REQUEST';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ROUTER = 'ROUTER';
}
