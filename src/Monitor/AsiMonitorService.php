<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Monitor;

//
use Jantia\Plugin\Monitor\MonitorService;
use Jantia\Plugin\Monitor\Std\MetricsPerformance;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiMonitorService extends MonitorService {
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_metricValues = [MetricsPerformance::PROCESS, MetricsPerformance::PERFORMANCE];
}
