<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Define ASI model layers
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum AsiTypeLayer: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case APPLICATION = 'APPLICATION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM = 'CUSTOM';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DATALINK = 'DATALINK';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case NETWORK = 'NETWORK';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PHYSICAL = 'PHYSICAL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PRESENTATION = 'PRESENTATION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SESSION = 'SESSION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TRANSPORT = 'TRANSPORT';
	
	/**
	 * @param    AsiTypeLayer    $layer
	 *
	 * @return null|AsiTypeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function toInterface(AsiTypeLayer $layer) : AsiTypeInterface|null {
		return match ( $layer ) {
			self::DATALINK, self::NETWORK, self::PHYSICAL => AsiTypeInterface::KERNEL,
			self::TRANSPORT => AsiTypeInterface::TRANSFER,
			self::APPLICATION, self::PRESENTATION, self::SESSION => AsiTypeInterface::SOFTWARE,
			self::CUSTOM => NULL
		};
	}
	
	/**
	 * @param    AsiTypeLayer    $layer
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDescription(AsiTypeLayer $layer) : string {
		return match ( $layer ) {
			self::CUSTOM => "Custom user-defined layer which can be used from any interface.",
			self::DATALINK => "Variable and data handling & validation.",
			self::NETWORK => "Prepare all things ready for Transport including route resolving etc.",
			self::PHYSICAL => "Gateway to OS level applications. Communication with Apache, Nginx, Docker, HAProxy etc.",
			self::TRANSPORT => "The Platform Router decides which path is used and which data is used. Router is based on MVC (Model-View-Controller) model.
			Including also Data Access Layer (DAL) which will handle data via adapters and interfaces.",
			self::APPLICATION => "Application Webhook and/or UI",
			self::PRESENTATION => "Application data layer which ensures that data is in usable format and data encryption occurs.",
			self::SESSION => "Maintains connections and is responsible for controlling data adapters and interfaces."
		};
	}
}
