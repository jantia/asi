<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Asi Model Interface types
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum AsiTypeInterface: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case BOOTSTRAP = 'BOOTSTRAP';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM = 'CUSTOM';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case KERNEL = 'KERNEL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case INTERFACE_NAME = 'INTERFACE_NAME';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SOFTWARE = 'SOFTWARE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TRANSFER = 'TRANSFER';
	
	/**
	 * @param    AsiTypeInterface    $interfaceType
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function toLayer(AsiTypeInterface $interfaceType) : array {
		return match ( $interfaceType ) {
			self::BOOTSTRAP => [],
			self::KERNEL => [AsiTypeLayer::DATALINK, AsiTypeLayer::NETWORK, AsiTypeLayer::PHYSICAL],
			self::SOFTWARE => [AsiTypeLayer::APPLICATION, AsiTypeLayer::SESSION, AsiTypeLayer::PRESENTATION],
			self::TRANSFER => [AsiTypeLayer::TRANSPORT],
			default => []
		};
	}
	
	/**
	 * @param    AsiTypeInterface    $interfaceType
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getDescription(AsiTypeInterface $interfaceType) : string {
		return match ( $interfaceType ) {
			self::BOOTSTRAP => "Default bootstrap which will run the whole default Tiat Framework + Jantia Platform process",
			self::CUSTOM => "Custom user-defined interface",
			self::KERNEL => "Will construct the system ready for Software interface. Will check/sort params etc.",
			self::SOFTWARE => "Application interface.",
			self::TRANSFER => "Transfer data between Kernel and Software interfaces."
		};
	}
}
