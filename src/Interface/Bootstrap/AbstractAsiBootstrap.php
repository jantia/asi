<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Bootstrap;

//
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Asi\Std\AsiTypeInterface;
use Jantia\Standard\Asi\Interface\AsiBootstrapInterface;
use Psr\Log\LoggerInterface;
use Tiat\Collection\Uri\UriHost;

use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiBootstrap extends AbstractAsi implements AsiBootstrapInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INTERFACE_NAME = AsiTypeInterface::BOOTSTRAP->value;
	
	/**
	 * Define default platform value (use Jantia)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string PLATFORM_DEFAULT = 'JANTIA';
	
	/**
	 * @param    null|iterable    $params
	 * @param    bool             $autoinit
	 * @param    bool             $autorun
	 * @param                     ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		// Set platform defined
		$this->setPlatform();
		
		//
		parent::__construct($params, $autoinit, $autorun, ...$args);
	}
	
	/**
	 * Register custom error handler. In this case the default is Jantia/Logit
	 * If you like to use other custom error handler then override this method
	 *
	 * @param    LoggerInterface    $logger
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function registerErrorHandler(LoggerInterface $logger) : void;
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConstruct(...$args) : void {
		//
		parent::_setConstruct(...$args);
		
		// Name can not be empty so use the default
		if($this->getName() === NULL):
			$this->setName(( $this->getDefaultRegister() )->getParam(AsiRegisterParams::APPLICATION_NAME->value) ??
			               ( new UriHost($_SERVER) )->getHost());
		endif;
		
		// Check that name is valid
		if($this->checkName($this->getName()) === FALSE):
			// Check that the name is valid
			$msg =
				sprintf("The bootstrap must have an valid name and consist (visibly) printable characters. Now '%s' has been given.",
				        $this->getName());
			throw new RuntimeException($msg);
		endif;
	}
}
