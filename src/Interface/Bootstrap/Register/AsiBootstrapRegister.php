<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Bootstrap\Register;

/**
 * This register will contain ALL global settings
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiBootstrapRegister extends AbstractAsiBootstrapRegister {

}
