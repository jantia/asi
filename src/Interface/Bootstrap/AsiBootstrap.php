<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Bootstrap;

//
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Register\AsiRegisterParams;
use Psr\Log\LoggerInterface;
use Tiat\Standard\Loader\ClassLoaderInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiBootstrap extends AbstractAsiBootstrap {
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		// Don't autoinit and/or autorun from parent class
		parent::__construct($params, FALSE, FALSE, ...$args);
		
		// Get register interface
		if(( $register = $this->getDefaultRegister() ) !== NULL &&
		   ( $log = $register->getParam(AsiRegisterParams::ERROR_HANDLER->value) ) !== NULL):
			// Define default error handler for the application
			$this->registerErrorHandler($log);
		endif;
		
		// Autoinit & autorun
		$this->_defineAutostart($autoinit, $autorun, ...$args);
	}
	
	/**
	 * @param    LoggerInterface    $logger
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function registerErrorHandler(LoggerInterface $logger) : void {
		$this->setLog($logger);
	}
	
	/**
	 * @param ...$args
	 *
	 * @return AsiBootstrap
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		// Complete init()
		if($this->isInitCompleted() === FALSE):
			if($this->getAutoInit() === TRUE):
				$this->init();
			else:
				throw new RuntimeException("Bootstrap initialization is missing.");
			endif;
		endif;
		
		// Define this interface register
		// Run AbstractAsi::run();
		return parent::run(...$args);
	}
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : ClassLoaderInterface {
		//
		$this->setInitCompleted(TRUE);
		
		//
		return $this;
	}
	
}
