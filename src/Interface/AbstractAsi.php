<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface;

//
use AllowDynamicProperties;
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Helper\AsiProcessHelperTrait;
use Jantia\Asi\Monitor\AsiMonitorService;
use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Asi\Std\AsiTypeInterface;
use Jantia\Asi\Version\AsiVersionPackage;
use Jantia\Standard\Asi\AsiProcessInterface;
use Jantia\Standard\Platform\PlatformEnv;
use Psr\Log\LogLevel;
use ReflectionClass;
use Tiat\Collection\Uri\Ip;
use Tiat\Collection\Uri\UriHost;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Monitor\MonitorInterface;
use Tiat\Stdlib\Register\Register;
use Tiat\Stdlib\Response\ResponseStatus;

use function constant;
use function defined;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
#[AllowDynamicProperties] abstract class AbstractAsi extends Register implements AsiProcessInterface, AbstractAsiInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_MONITOR_INTERFACE = AsiMonitorService::class;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_MONITOR_PACKAGE = AsiVersionPackage::class;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string INFO_PACKAGE_MONITORING = 'info_package_monitoring';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string INFO_INTERFACES_COMPLETED = 'info_interfaces_completed';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiProcessHelperTrait;
	
	/**
	 * @var LogLevel
	 * @since   3.0.0 First time introduced.
	 */
	private LogLevel $_outputLevel;
	
	/**
	 * @var int|false
	 * @since   3.0.0 First time introduced.
	 */
	private int|false $_outputCode;
	
	/**
	 * @var MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	private MonitorInterface $_monitorInterface;
	
	/**
	 * @var array|string[][]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_templates = [LogLevel::INFO => [self::INFO_PACKAGE_MONITORING => "Package monitoring results.",
	                                                self::INFO_INTERFACES_COMPLETED => "All interfaces have been parsed."]];
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		// Add templates
		$name = ( new ReflectionClass($this) )->getParentClass()->getShortName();
		$this->setMessageTemplates($this->_templates);
		
		//
		parent::__construct(...$args);
		
		//
		$this->_setConstruct(...$args);
		
		//
		$this->_defineAutostart($autoinit, $autorun, ...$args);
	}
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConstruct(...$args) : void {
		// Start monitoring the application
		$this->startMonitoring($args[AsiRegisterParams::MONITORING_STATUS->value] ?? FALSE);
		
		// Resolve the name of this container from Register (if possible)
		if(! empty($register = $args[AsiTypeInterface::INTERFACE_NAME->value] ??
		                       $this->getClassConstant(AsiTypeInterface::INTERFACE_NAME->value) ?? NULL) &&
		   ( $default = $this->getDefaultRegister() ) !== NULL):
			$this->setName(strtolower($register));
		endif;
		
		// Use default name if none exists
		if(empty($this->getName())):
			$this->setName(( new ReflectionClass($this) )->getParentClass()->getShortName());
		endif;
	}
	
	/**
	 * @param    bool    $monitoring
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function startMonitoring(bool $monitoring = FALSE) : void {
		// Start metrics
		if($monitoring === TRUE || ( ( ( $register = $this->getDefaultRegister() ) !== NULL ) &&
		                             $register->getParam(AsiRegisterParams::MONITORING_STATUS->value) === TRUE )):
			// Start monitoring
			$p               = self::DEFAULT_MONITOR_PACKAGE;
			$args['package'] = new $p();
			$this->getMonitorInterface(...$args)?->setName((string)static::class)->start();
		endif;
	}
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return null|MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitorInterface(...$args) : ?MonitorInterface {
		if(empty($this->_monitorInterfaface)):
			$this->setMonitorInterface(...$args);
		endif;
		
		//
		return $this->_monitorInterface ?? NULL;
	}
	
	/**
	 * @param    NULL|MonitorInterface    $monitor
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setMonitorInterface(?MonitorInterface $monitor = NULL, ...$args) : static {
		//
		if($monitor === NULL && empty($this->_monitorInterface)):
			$class   = self::DEFAULT_MONITOR_INTERFACE;
			$monitor = new $class(...$args);
		endif;
		
		//
		if($monitor instanceof MonitorInterface):
			$this->_monitorInterface = $monitor;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return AbstractAsi
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ClassLoaderInterface {
		// Check if the autoinit is enabled & run it if required
		$this->classLoaderRun(...$args);
		
		// Use interface name as the root or throw an exception
		if(! empty($name = $args[AsiTypeInterface::INTERFACE_NAME->value] ??
		                   $this->getClassConstant(AsiTypeInterface::INTERFACE_NAME->value))):
			if($this->isInitCompleted() === TRUE):
				// Move to parent::run
				return $this;
			else:
				$msg = sprintf("Init must be completed before running this %s.", $name);
				throw new RuntimeException($msg);
			endif;
		else:
			# All done!
			$msg = sprintf("All interfaces & layers have been parsed by %s.", $this->getName());
			$this->setMessage(self::INFO_PACKAGE_MONITORING, message:$msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function __invoke(...$args) : static {
		//
		$this->_setConstruct(...$args);
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
		$this->stopMonitoring();
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 * @since   3.0.2 Remove dependency from Jantia/Logit and use UriHost()->getHost() instead.
	 */
	public function stopMonitoring() : void {
		// Stop monitoring
		$this->getMonitorInterface()?->stop();
		
		// Get monitoring result (if any)
		if(! empty($result = $this->getMonitorInterface()?->getResult())):
			// Reset result's so these are not logged multiple times
			$this->getMonitorInterface()?->resetResult();
			
			//
			if(defined(PlatformEnv::TRACE_ID->value) === TRUE):
				//
				$host['host']           = [( new UriHost($_SERVER) )->getHost()];
				$host['host']['server'] = $_SERVER['SERVER_ADDR'];
				$host['host']['client'] = ( new Ip() )->clientIp();
				$result                 =
					[...['trace' => constant(PlatformEnv::TRACE_ID->value)], ...$host, ...$result];
				
				// Add to backend logs
				$this->setMessage(self::INFO_PACKAGE_MONITORING, message:$result);
			else:
				throw new RuntimeException("Trace ID is missing, please check that it exists as constant.");
			endif;
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMonitorInterface() : static {
		//
		unset($this->_monitorInterface);
		
		//
		return $this;
	}
	
	/**
	 * @return int|false
	 * @since   3.0.0 First time introduced.
	 */
	public function getOutputCode() : int|false {
		return $this->_outputCode ?? FALSE;
	}
	
	/**
	 * @param    int    $code
	 *
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOutputCode(int $code) : AsiProcessInterface {
		//
		if(! empty(ResponseStatus::checkStatusCode($code))):
			$this->_outputCode = $code;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return LogLevel
	 * @since   3.0.0 First time introduced.
	 */
	public function getOutputLevel() : LogLevel {
		return $this->_outputLevel;
	}
	
	/**
	 * @param    LogLevel    $level
	 *
	 * @return AsiProcessInterface
	 */
	public function setOutputLevel(LogLevel $level) : AsiProcessInterface {
		//
		$this->_outputLevel = $level;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function outputError() : ?string {
		// TODO: Implement outputError() method.
		return NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function outputResult() : ?string {
		// TODO: Implement outputResult() method.
		return NULL;
	}
	
	/**
	 * @param    int    $condition
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function throwErrorIf(int $condition) : void {
		// TODO: Implement throwErrorIf() method.
	}
	
	/**
	 * @param    int    $timeout
	 *
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function timeout(int $timeout) : AsiProcessInterface {
		// TODO: Implement timeout() method.
		
		return $this;
	}
}
