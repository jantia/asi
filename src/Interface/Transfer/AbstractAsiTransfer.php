<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Transfer;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Std\AsiTypeInterface;
use Jantia\Standard\Asi\Interface\AsiTransferInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiTransfer extends AbstractAsi implements AsiTransferInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INTERFACE_NAME = AsiTypeInterface::TRANSFER->value;
}
