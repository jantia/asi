<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Transfer;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Interface\Kernel\AbstractAsiKernel;
use Jantia\Asi\Register\AsiRegisterModules;
use Jantia\Asi\Register\AsiRegisterParams;
use Psr\Http\Message\RequestInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Request\RequestElement;

use function is_callable;
use function is_object;
use function is_string;
use function key;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiTransfer extends AbstractAsiTransfer {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouter(...$args) : static {
		//
		$options = $args[key($args)];
		
		//
		if(( $handler = $options[AsiRegisterParams::HANDLER->value] ?? NULL ) !== NULL):
			//
			$contract = $options[AsiRegisterParams::CONTRACT->value] ?? NULL;
			$params   = $options[AsiRegisterParams::PARAMS->value] ?? [];
			
			//
			if(is_string($handler)):
				//
				$router = new $handler(...$params);
				
				// Set URL for router
				if($router instanceof ParametersPluginInterface):
					$router->setParam('url', $this->getUrl(), TRUE);
				endif;
				
				//
				$settings = $options[AsiRegisterParams::SETTINGS->value] ?? [];
				
				//
				if(! empty($settings) && $settings instanceof ParametersPluginInterface && is_callable($router)):
					$this->setSettings($router, $settings->getParams());
				endif;
			elseif(is_object($handler)):
				$router = $handler;
			else:
				throw new RuntimeException("Router handler is not defined.");
			endif;
			
			//
			if(( $contract !== NULL ) && ! $router instanceof $contract):
				$msg = sprintf("Router %s must be an instance of %s.", $handler, $contract);
				throw new InvalidArgumentException($msg);
			endif;
			
			// Get message bus
			if(( $bus = $this->getMessageBusDefault() ) !== NULL &&
			   ( $dto = $bus->getMessage(AsiRegisterModules::REQUEST->value) ) !== NULL):
				//
				$request = $dto->getData();
				
				// Set request path as a route for Router (as string)
				$router->setRoute($this->getRequestElement(RequestElement::PATH, $request));
				
				if($request instanceof RequestInterface):
					$router->setRequest($request);
				endif;
			endif;
			
			// Set user input variables to router
			if(( $register = $this->getApplicationRegister() ) !== NULL):
				//
				$e = $this->getNamedRegister($register[AbstractAsiKernel::INTERFACE_NAME]);
				
				//
				if($e instanceof RegisterInterface):
					$router->setParams([self::VARIABLES_USER_INPUT => $e->getParam('userInput')]);
				endif;
			endif;
			
			// Run the router
			$router->run();
		else:
			throw new RuntimeException("No router defined in settings");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url ?? NULL;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setUrl(string $url) : static {
		//
		$this->_url = $url;
		
		//
		return $this;
	}
	
	/**
	 * @param    null|RequestElement      $key
	 * @param    null|RequestInterface    $request
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestElement(?RequestElement $key = NULL, ?RequestInterface $request = NULL) : ?string {
		// Real url can be found from $request
		if($request !== NULL):
			$url = $request->getUri();
			
			return match ( $key ) {
				RequestElement::PATH => $url->getPath(),
				RequestElement::QUERY => $url->getQuery(),
				default => throw new InvalidArgumentException(sprintf("Unsupported request element: %s",
				                                                      $key->value ?? '')),
			};
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
}
