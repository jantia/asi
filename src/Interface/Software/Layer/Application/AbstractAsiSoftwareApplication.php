<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Software\Layer\Application;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Std\AsiTypeLayer;
use Jantia\Standard\Asi\Layer\Software\AsiSoftwareApplicationInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiSoftwareApplication extends AbstractAsi implements AsiSoftwareApplicationInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INTERFACE_NAME = AsiTypeLayer::APPLICATION->value;
}
