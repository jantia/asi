<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Software\Layer\Session\Register;

//
use Jantia\Asi\Interface\AbstractAsiRegister;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiSessionRegister extends AbstractAsiRegister {

}
