<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Software;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Std\AsiTypeInterface;
use Jantia\Standard\Asi\Interface\AsiSoftwareInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiSoftware extends AbstractAsi implements AsiSoftwareInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INTERFACE_NAME = AsiTypeInterface::SOFTWARE->value;
}
