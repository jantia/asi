<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Software;

//
use Tiat\Standard\Loader\ClassLoaderInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiSoftware extends AbstractAsiSoftware {
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ClassLoaderInterface {
		//
		parent::run($args);
		
		//
		return $this;
	}
}
