<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AbstractAsiInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string VARIABLES_USER_INPUT = 'userInput';
}