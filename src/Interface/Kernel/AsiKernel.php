<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel;

//
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Register\AsiRegisterModules;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Stream;
use Tiat\Collection\Dto\DataTransferObject;
use Tiat\Router\Request\Request;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiKernel extends AbstractAsiKernel {
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ClassLoaderInterface {
		//
		parent::run($args);
		
		// Set Message Bug interface if not exists
		if($this->getMessageBus() === NULL && ( $bus = $this->getMessageBusDefault() ) !== NULL):
			$this->setMessageBus($bus);
		endif;
		
		// Create and initialize the Request object
		$request = new Request();
		
		// Initialize the Request with data from Kernel interface
		if(( $register = $this->getApplicationRegister() ) !== NULL):
			// Get Kernel register which is a container
			$e = $this->getNamedRegister(strtolower($register[AbstractAsiKernel::INTERFACE_NAME]));
			if($e instanceof ParametersPluginInterface):
				// Use Laminas Diactoros ServerRequestFactory to create a ServerRequest instance
				// @TODO Merge this with AsiKernelDatalink & connect to use Validator's
				$sr = ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);
				
				// Register the POST data to $request
				$s = new Stream('php://temp', 'wb+');
				$s->write($sr->getBody()->getContents());
				$s->rewind();
				
				// Initialize the request & get the CLONE of the request
				$request->initialize($e->getParam('url'), $e->getParam('method'), 'php://temp', $sr->getHeaders());
				$request = $request->withBody($s);
			endif;
		endif;
		
		// Save the request to MessageBus (Notice! The Request is not the CLONE of the original Request)
		$this->setRequestInterface($request);
		if(( $bus = $this->getMessageBus() ) !== NULL):
			$bus->setMessage(AsiRegisterModules::REQUEST->value, new DataTransferObject($this->getRequestInterface()));
		else:
			throw new RuntimeException("Message bus is not set.");
		endif;
		
		//
		return $this;
	}
}
