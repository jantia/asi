<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Std\AsiTypeInterface;
use Jantia\Standard\Asi\Interface\AsiKernelInterface;
use Psr\Http\Message\RequestInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiKernel extends AbstractAsi implements AsiKernelInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INTERFACE_NAME = AsiTypeInterface::KERNEL->value;
	
	/**
	 * @var RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	private RequestInterface $_requestInterface;
	
	/**
	 * @return null|RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestInterface() : ?RequestInterface {
		return $this->_requestInterface ?? NULL;
	}
	
	/**
	 * @param    RequestInterface    $request
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequestInterface(RequestInterface $request) : void {
		//
		$this->_requestInterface = $request;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRequestInterface() : void {
		unset($this->_requestInterface);
	}
}
