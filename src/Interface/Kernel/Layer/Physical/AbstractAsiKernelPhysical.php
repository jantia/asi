<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel\Layer\Physical;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Standard\Asi\Layer\Kernel\AsiKernelPhysicalInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiKernelPhysical extends AbstractAsi implements AsiKernelPhysicalInterface {
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _initPhysical() : void {
	
	}
}
