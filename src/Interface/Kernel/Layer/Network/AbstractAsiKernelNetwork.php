<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel\Layer\Network;

//
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Standard\Asi\Layer\Kernel\AsiKernelNetworkInterface;
use Tiat\Standard\Request\RequestInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiKernelNetwork extends AbstractAsi implements AsiKernelNetworkInterface {
	
	/**
	 * @var RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	private readonly RequestInterface $_requestInterface;
	
	/**
	 * @return null|RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestInterface() : ?RequestInterface {
		if(! empty($this->_requestInterface)):
			return $this->_requestInterface;
		else:
			throw new RuntimeException("Request interface doesn't exists.");
		endif;
	}
	
	/**
	 * @param    RequestInterface    $request
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequestInterface(RequestInterface $request) : static {
		//
		$this->_requestInterface = $request;
		
		//
		return $this;
	}
}
