<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel\Layer\Network;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Asi\Exception\RuntimeException;
use Jantia\Asi\Interface\Kernel\AbstractAsiKernel;
use Laminas\Uri\UriFactory;
use Psr\Http\Message\UriInterface;
use Tiat\Collection\Uri\UriHost;
use Tiat\Standard\DataModel\HttpMethod;
use Tiat\Standard\DataModel\HttpMethodCustom;
use Tiat\Standard\DataModel\HttpMethodType;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Request\RequestElement;
use WeakMap;

use function in_array;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiKernelNetwork extends AbstractAsiKernelNetwork {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_customMethods = HttpMethodCustom::class;
	
	/**
	 * @var WeakMap
	 * @since   3.0.0 First time introduced.
	 */
	private WeakMap $_requestUrl;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_httpMethod;
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		// We need valid URI for Router
		// Exception will be raised by _validateUrl() method if not valid
		if(! empty($url = ( new UriHost($_SERVER) )->getUrl()) && $this->_validateUrl($url ?? '')):
			//
			$this->setUrl($url);
			
			//
			$this->setMethod($args['method'] ?? $_SERVER['REQUEST_METHOD']);
			
			//
			$this->setInitCompleted(TRUE);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url ?? NULL;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setUrl(string $url) : static {
		//
		$this->_url = $url;
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $url
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _validateUrl(?string $url = NULL) : bool {
		// Validate the hostname
		if(( $uri = UriFactory::factory($url ?? $this->getUrl()) ) && $uri->isValid()):
			//
			$this->setUrl($uri->toString());
			
			//
			if(empty($this->_requestUrl)):
				$this->_requestUrl = new WeakMap();
			endif;
			
			//
			$this->_requestUrl->offsetSet(RequestElement::PATH, $uri->getPath());
			$this->_requestUrl->offsetSet(RequestElement::QUERY, $uri->getQueryAsArray());
			$this->_requestUrl->offsetSet(RequestElement::HOST, $uri->getHost());
			$this->_requestUrl->offsetSet(RequestElement::SCHEME, $uri->getScheme());
			$this->_requestUrl->offsetSet(RequestElement::PORT, $uri->getPort());
			$this->_requestUrl->offsetSet(RequestElement::USERINFO, $uri->getUserInfo());
			$this->_requestUrl->offsetSet(RequestElement::FRAGMENT, $uri->getFragment());
			
			//
			return TRUE;
		endif;
		
		//
		$msg = sprintf('Url is not valid (%s)', $url);
		throw new RuntimeException($msg);
	}
	
	/**
	 * @param    null|string    $method
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setMethod(?string $method = NULL) : static {
		//
		if(empty($method)):
			$method = $_SERVER['REQUEST_METHOD'];
		endif;
		
		//
		$this->_httpMethod = strtoupper($method);
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ClassLoaderInterface {
		//
		parent::run($args);
		
		//
		if(( $this->hasRun() === FALSE ) && ( $register = $this->getApplicationRegister() ) !== NULL):
			//
			$e = $this->getNamedRegister($register[AbstractAsiKernel::INTERFACE_NAME]);
			if($e instanceof ParametersPluginInterface):
				$e->setParam('url', $this->getUrl());
				$e->setParam('method', $this->getMethod());
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getMethod() : string {
		return $this->_httpMethod;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
	
	// Start the deprecated
	
	/**
	 * @param    NULL|RequestElement    $key
	 *
	 * @return null|WeakMap|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestElement(?RequestElement $key = NULL) : WeakMap|array|string|null {
		if($key !== NULL):
			if($key === RequestElement::PORT):
				return $this->_requestUrl->offsetGet($key);
			elseif($key === RequestElement::QUERY):
				return $this->_requestUrl->offsetGet($key) ?? [];
			else:
				return $this->_requestUrl->offsetGet($key) ?? '';
			endif;
		else:
			return $this->_requestUrl ?? NULL;
		endif;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function resolveMethodFromUrlCustom(string $url) : ?string {
		return NULL;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getCustomMethodFromUrl(string $url) : ?string {
		//
		if(( ( $uri = UriFactory::factory($url) ) instanceof UriInterface ) && $uri->isValid() &&
		   ! empty($custom = $uri->getMethod())):
			return strtoupper($custom);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    array    $values
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkMethodValues(array $values) : bool {
		if(! empty($valid = HttpMethod::toArray(HttpMethodType::toMethod(HttpMethodType::VALID)))):
			foreach($values as $val):
				$val = mb_strtoupper($val, $this->getEncoding());
				if(! in_array($val, $valid, TRUE)):
					$msg = sprintf("Method '%s' is not accepted.", $val);
					throw new InvalidArgumentException($msg);
				endif;
			endforeach;
			
			//
			return TRUE;
		endif;
		
		//
		throw new RuntimeException('No valid methods available.');
	}
}
