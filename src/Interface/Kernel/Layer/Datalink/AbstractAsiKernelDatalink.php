<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel\Layer\Datalink;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Std\AsiTypeLayer;
use Jantia\Standard\Asi\Layer\Kernel\AsiKernelDatalinkInterface;
use Laminas\Validator\ValidatorInterface;
use Tiat\Standard\DataModel\VariableElement;

use function array_diff;
use function array_keys;
use function implode;
use function mb_strtolower;
use function str_split;
use function strtoupper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiKernelDatalink extends AbstractAsi implements AsiKernelDatalinkInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INTERFACE_NAME = AsiTypeLayer::DATALINK->value;
	
	/**
	 * Default order for user variables
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_USER_VARIABLES_ORDER = 'GPC';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array USER_INPUT_VARS = [VariableElement::GET->value => [], VariableElement::POST->value => [],
	                                            VariableElement::COOKIE->value => [],
	                                            VariableElement::HEADERS->value => [],
	                                            VariableElement::FILES->value => [],
	                                            VariableElement::SERVER->value => [],
	                                            VariableElement::ENV->value => [],];
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_validators = [];
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_validationErrors;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_caseSensitive = FALSE;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_variablesOrder = self::DEFAULT_USER_VARIABLES_ORDER;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getVariablesOrder() : string {
		return $this->_variablesOrder;
	}
	
	/**
	 * @param    string    $order
	 * @param    bool      $register
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setVariablesOrder(string $order = 'GPC', bool $register = TRUE) : static {
		// Get the current order
		$current = $this->getVariablesOrder();
		$order   = strtoupper($order);
		
		//
		$this->_variablesOrder = $order;
		
		// Trigger the variables order change but don't override params which exists
		if($current !== $order && $register === TRUE):
			//
			$this->registerVariables($this->getVariablesOrder());
			
			// Clean all those vars which are not used in new order
			if(! empty($diff = array_diff(str_split($current), str_split($order)))):
				$this->unregisterVariables(implode('', $diff));
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetVariablesOrder() : static {
		//
		$this->_variablesOrder = self::DEFAULT_USER_VARIABLES_ORDER;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getValidators() : array|null {
		return $this->_validators ?? NULL;
	}
	
	/**
	 * @param    array    $validators
	 *
	 * @return AsiKernelDatalink
	 * @since   3.0.0 First time introduced.
	 */
	public function setValidators(array $validators) : static {
		//
		foreach($validators as $key => $val):
			$this->setValidator($key, $val);
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                      $key
	 * @param    ValidatorInterface|array    $validator
	 *
	 * @return AsiKernelDatalink
	 * @since   3.0.0 First time introduced.
	 */
	public function setValidator(string $key, ValidatorInterface|array $validator) : static {
		//
		$this->_validators[$key] = $validator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getValidator(string $key) : array|null {
		if(! empty($this->_validators)):
			if($this->isCaseSensitive() === FALSE):
				$keys = array_keys($this->_validators);
				foreach($keys as $val):
					if(mb_strtolower($val, $this->getEncoding()) === mb_strtolower($key, $this->getEncoding())):
						return $this->_validators[$val];
					endif;
				endforeach;
			else:
				return $this->_validators[$key] ?? NULL;
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isCaseSensitive() : bool {
		return $this->_caseSensitive;
	}
	
	/**
	 * @param    bool    $caseSensitive
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setCaseSensitive(bool $caseSensitive) : static {
		//
		$this->_caseSensitive = $caseSensitive;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetValidators() : static {
		//
		unset($this->_validators);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteValidator(string $key) : static {
		//
		unset($this->_validators[$key]);
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getValidationError() : array {
		return $this->_validationErrors ?? [];
	}
	
	/**
	 * @param    string          $key
	 * @param    array|string    $messages
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setValidationError(string $key, array|string $messages) : void {
		$this->_validationErrors[$key] = [...$this->_validationErrors[$key] ?? [], (array)$messages];
	}
}
