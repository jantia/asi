<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface\Kernel\Layer\Datalink;

//
use Jantia\Asi\Exception\InvalidArgumentException;
use Jantia\Asi\Interface\Kernel\AbstractAsiKernel;
use Laminas\Validator\ValidatorInterface;
use Tiat\Config\File\AbstractDefaultConfigFile;
use Tiat\Standard\DataModel\VariableElement;
use Tiat\Standard\Parameters\ParametersInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\Parameters;
use UnitEnum;

use function array_merge_recursive;
use function filter_var;
use function getallheaders;
use function htmlspecialchars;
use function in_array;
use function is_array;
use function is_scalar;
use function is_string;
use function mb_strtoupper;
use function sprintf;
use function str_split;
use function trim;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AsiKernelDatalink extends AbstractAsiKernelDatalink {
	
	/**
	 * User input variables (validated)
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_userInput = self::USER_INPUT_VARS;
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		parent::run(...$args);
		
		// This must be after init()
		$this->registerVariables($this->getVariablesOrder());
		
		//
		if($this->validateUserInput() === TRUE):
			$this->_mergeVariables();
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|VariableElement    $order
	 * @param    bool                      $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function registerVariables(string|VariableElement $order = 'gpfces', bool $override = FALSE) : static {
		//
		if(empty($order)):
			$order = 'gpfces';
		endif;
		
		//
		$accept = VariableElement::toArray(VariableElement::cases());
		$order  = mb_strtoupper($order, $this->getEncoding());
		
		// Use single word
		if(in_array($order, $accept, TRUE)):
			$this->_registerVariables($order, $override);
		else:
			// Use the letters from string
			$letters = str_split($order);
			foreach($letters as $val):
				$this->_registerVariables($val, $override);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $source
	 * @param    bool      $override
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _registerVariables(string $source, bool $override = FALSE) : void {
		if(isset($source[0])):
			//
			$c = VariableElement::COOKIE->value;
			$e = VariableElement::ENV->value;
			$f = VariableElement::FILES->value;
			$g = VariableElement::GET->value;
			$p = VariableElement::POST->value;
			$s = VariableElement::SERVER->value;
			$h = VariableElement::HEADERS->value;
			
			// Source can be a single character or whole word
			match ( mb_strtoupper($source, $this->getEncoding()) ) {
				$c[0], $c => ( $this->setParam($c, ( new Parameters($_COOKIE) )->toArray(), override:$override,
					throwError:                FALSE) ),
				$e[0], $e => ( $this->setParam($e, ( new Parameters($_ENV) )->toArray(), override:$override,
					throwError:                FALSE) ),
				$f[0], $f => ( $this->setParam($f, new Parameters($this->_mapPhpFiles()), override:$override,
					throwError:                FALSE) ),
				$g[0], $g => ( $this->setParam($g, ( new Parameters($_GET) )->toArray(), override:$override,
					throwError:                FALSE) ),
				$p[0], $p => ( $this->setParam($p, ( new Parameters($_POST) )->toArray(), override:$override,
					throwError:                FALSE) ),
				$s[0], $s => ( $this->setParam($s, ( new Parameters($_SERVER) )->toArray(), override:$override,
					throwError:                FALSE) ),
				$h[0], $h => ( $this->setParam($h, ( new Parameters(getallheaders()) )->toArray(), override:$override,
					throwError:                FALSE) ),
				default => throw new InvalidArgumentException(sprintf('Given argument (%s) is not valid.', $source)),
			};
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	private function _mapPhpFiles() : array {
		//
		foreach($_FILES as $name => $file):
			//
			$files[$name] = [];
			
			//
			foreach($file as $param => $data):
				if(is_array($data)):
					foreach($data as $i => $v):
						$this->_mapPhpFileParam($files[$name], $param, $i, $v);
					endforeach;
				else:
					$files[$name][$param] = $data;
				endif;
			endforeach;
		endforeach;
		
		//
		return $files ?? [];
	}
	
	/**
	 * @param $array
	 * @param $name
	 * @param $index
	 * @param $value
	 *
	 * @since   3.0.0 First time introduced.
	 */
	private function _mapPhpFileParam(&$array, $name, $index, $value) : void {
		if(is_array($value)):
			foreach($value as $i => $v):
				$this->_mapPhpFileParam($array[$index], $name, $i, $v);
			endforeach;
		else:
			$array[$index][$name] = $value;
		endif;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function validateUserInput() : bool {
		//
		if(! empty($order = str_split($this->getVariablesOrder()))):
			$c = VariableElement::COOKIE->value;
			$e = VariableElement::ENV->value;
			$f = VariableElement::FILES->value;
			$g = VariableElement::GET->value;
			$p = VariableElement::POST->value;
			$s = VariableElement::SERVER->value;
			$h = VariableElement::HEADERS->value;
			
			//
			foreach($order as $val):
				match ( mb_strtoupper($val, $this->getEncoding()) ) {
					$c[0], $c => $this->_userInput[$c] = $this->_validateVariables($this->getParam($c)),
					$e[0], $e => $this->_userInput[$e] = $this->_validateVariables($this->getParam($e)),
					$f[0], $f => $this->_userInput[$f] = $this->_validateVariables($this->getParam($f)),
					$g[0], $g => $this->_userInput[$g] = $this->_validateVariables($this->getParam($g)),
					$p[0], $p => $this->_userInput[$p] = $this->_validateVariables($this->getParam($p)),
					$s[0], $s => $this->_userInput[$s] = $this->_validateVariables($this->getParam($s)),
					$h[0], $h => $this->_userInput[$h] = $this->_validateVariables($this->getParam($h)),
				};
			endforeach;
			
			//
			$this->validateVariableWithValidators($this->_userInput);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * Validate & sanitize user input
	 *
	 * @param    array    $vars
	 *
	 * @return ParametersInterface|array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _validateVariables(mixed $vars) : ParametersInterface|array {
		if(! empty($vars)):
			foreach($vars as $key => &$val):
				// Check encoding
				$val = $this->_validateVariableEncoding($key, $val);
				
				// Sanitize the value
				if(is_string($val)):
					$val = $this->_sanitizeVariable($val);
				endif;
			endforeach;
		endif;
		
		//
		return $vars;
	}
	
	/**
	 * @param    string|int    $key
	 * @param    mixed         $value
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	protected function _validateVariableEncoding(string|int $key, mixed $value) : mixed {
		//
		if(! is_string($key)):
			$key = (string)$key;
		endif;
		
		// Check also key encoding and raise an error if encoding is not supported.
		if($this->checkEncoding($key, $this->getEncoding()) === TRUE):
			if(is_string($value) || is_array($value)):
				if($this->checkEncoding($value, $this->getEncoding()) === FALSE):
					$this->_setValidationError($key, 'Encoding error.');
				endif;
			endif;
		endif;
		
		//
		return $value;
	}
	
	/**
	 * @param    mixed    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _sanitizeVariable(string $value) : string {
		//
		$value = trim($value);
		
		//
		if(! empty($value)):
			$converted = htmlspecialchars($value, ENT_NOQUOTES | ENT_HTML5, $this->getEncoding(), FALSE);
			
			// Set default options for all filters
			$options = ['options' => ['default' => NULL, 'flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]];
			
			//
			return filter_var(filter_var($converted, FILTER_SANITIZE_ENCODED, $options), FILTER_SANITIZE_SPECIAL_CHARS,
			                  $options);
		endif;
		
		//
		return $value;
	}
	
	/**
	 * @param    ParametersInterface|array    $input
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function validateVariableWithValidators(ParametersInterface|array $input) : void {
		//
		$cases = VariableElement::toArray(VariableElement::cases());
		
		if(! empty($input) || ( $input instanceof ParametersInterface && $input->count() > 0 )):
			foreach($input as $key => $val):
				//
				if(in_array($key, $cases, TRUE) === TRUE):
					if(! empty($validators = $this->getValidator($key))):
						$this->_validateVariableWithValidators($key, $val, $validators);
					else:
						$this->validateVariableWithValidators($val);
					endif;
				elseif(! empty($validators = $this->getValidator($key))):
					$this->_validateVariableWithValidators($key, $val, $validators);
				endif;
			endforeach;
		endif;
	}
	
	/**
	 * @param    string        $key
	 * @param    mixed         $value
	 * @param    null|array    $validators
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _validateVariableWithValidators(string $key, mixed $value, ?array $validators = NULL) : bool {
		// Get validators if not defined
		if(empty($validators)):
			$validators = $this->getValidator($key);
		endif;
		
		// Check only scalar values
		if(is_scalar($value)):
			foreach($validators as $val):
				// Reset validator
				$v = NULL;
				
				// Define validator
				if(is_array($val) && ( $class = $val[AbstractDefaultConfigFile::VALIDATOR_CLASS] ?? NULL ) !== NULL):
					$options = $val[AbstractDefaultConfigFile::VALIDATOR_OPTIONS] ?? [];
					$v       = new $class($options);
				elseif($val instanceof ValidatorInterface):
					$v = $val;
				endif;
				
				//
				if(( $v instanceof ValidatorInterface ) && $v->isValid($value) === FALSE):
					//
					$this->_setValidationError($key, $v->getMessages());
					
					//
					return FALSE;
				endif;
			endforeach;
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _mergeVariables() : void {
		//
		$input  = $this->getUserInput();
		$result = [];
		
		// Collect && merge data
		$cases = VariableElement::toArray(VariableElement::cases());
		foreach($cases as $key => $val):
			if(is_array($a = $input[$val]) && ! empty($a)):
				$result[$val] = $a;
			endif;
		endforeach;
		
		// Get not-validated params
		$errors    = $this->_getValidationError();
		$resultset = $this->_mergeVariablesErrors($result, $errors);
		
		//
		if(( $register = $this->getApplicationRegister() ) !== NULL):
			// Get Kernel register
			$e = $this->getNamedRegister($register[AbstractAsiKernel::INTERFACE_NAME]);
			if($e instanceof ParametersPluginInterface):
				$e->setParam(self::VARIABLES_USER_INPUT, $resultset ?? [], TRUE, FALSE);
			endif;
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getUserInput() : array {
		return $this->_userInput;
	}
	
	/**
	 * @param    array    $result
	 * @param    array    $errors
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	private function _mergeVariablesErrors(array $result, array $errors) : array {
		if(! empty($errors)):
			foreach($errors as $key => $val):
				foreach($result as $method => $value):
					if(isset($value[$key])):
						$resultset['errors'][$key] = ['value' => $value[$key], 'errors' => $val];
						unset($result[$method][$key]);
					endif;
				endforeach;
			endforeach;
			
			//
			$resultset['validated'] = $result;
		endif;
		
		//
		return $resultset ?? $result;
	}
	
	/**
	 * @param    string|VariableElement    $order
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function unregisterVariables(string|VariableElement $order) : static {
		//
		$accept = VariableElement::toArray(VariableElement::cases());
		$order  = mb_strtoupper($order, $this->getEncoding());
		
		//
		if(in_array($order, $accept, TRUE)):
			$this->_unregisterVariables($order);
		else:
			// Use the letters from string
			$letters = str_split($order);
			foreach($letters as $val):
				$this->_unregisterVariables($val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $source
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _unregisterVariables(string $source) : void {
		if(isset($source[0])):
			$c = VariableElement::COOKIE->value;
			$e = VariableElement::ENV->value;
			$f = VariableElement::FILES->value;
			$g = VariableElement::GET->value;
			$p = VariableElement::POST->value;
			$s = VariableElement::SERVER->value;
			$h = VariableElement::HEADERS->value;
			
			// Source can be a single character or whole word
			match ( mb_strtoupper($source, $this->getEncoding()) ) {
				$c[0], $c => $this->resetParam($c),
				$e[0], $e => $this->resetParam($e),
				$f[0], $f => $this->resetParam($f),
				$g[0], $g => $this->resetParam($g),
				$p[0], $p => $this->resetParam($p),
				$s[0], $s => $this->resetParam($s),
				$h[0], $h => $this->resetParam($h),
				default => throw new InvalidArgumentException(sprintf('Given argument (%s) is not valid.', $source)),
			};
		endif;
	}
	
	/**
	 * @param    NULL|string|VariableElement    $method
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUserInput(VariableElement|string|null $method = NULL) : static {
		//
		if(is_string($method)):
			unset($this->_userInput[$method]);
		elseif($method instanceof UnitEnum):
			unset($this->_userInput[$method->value]);
		else:
			unset($this->_userInput);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getValidatedVariables() : ?array {
		// Get validated params
		if(! empty($input = $this->getUserInput())):
			foreach($input as $val):
				$vars = array_merge_recursive($vars ?? [], $this->getParam($val) ?? []);
			endforeach;
		endif;
		
		//
		return $vars ?? NULL;
	}
}
