<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Asi
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Asi\Interface;

//
use Jantia\Asi\Helper\AsiProcessHelperTrait;
use Jantia\Asi\Register\AsiRegisterInterface;
use Tiat\Stdlib\Register\Register;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractAsiRegister extends Register implements AsiRegisterInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiProcessHelperTrait;
}
